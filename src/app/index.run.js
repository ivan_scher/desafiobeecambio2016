(function() {
  'use strict';

  angular
    .module('desafiobeecambio2016')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
