(function() {
  'use strict';

  describe('controllers', function(){
    var vm;

    beforeEach(module('desafiobeecambio2016'));
    beforeEach(inject(function(_$controller_ ) {
      vm = _$controller_('MainController');
    }));

    it('should have a object anuncios', function() {
      expect(vm.anuncios).toEqual(jasmine.any(Object));
    });

  });
})();
