(function() {
    'use strict';
    angular
        .module('desafiobeecambio2016').factory('MainService', function( $resource){

        return $resource('http://demo3292918.mockable.io/anuncio', { }, {
            query: {
                method: 'GET',
                transformResponse: function(data) {
                    return angular.fromJson(data).results;
                },
                isArray: true
            },
            save: {
                method: 'POST',
                headers: { 'auth-token': 'desafiobeecambio2016' }
            }
        });


    });

})();