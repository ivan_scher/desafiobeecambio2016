(function() {
    'use strict';
    angular
        .module('desafiobeecambio2016')
        .controller('ModalAddController', ModalAddController);
    /** @ngInject */
    function ModalAddController( $rootScope,$uibModalInstance, $log, MainService,toastr) {
        var vm = this;
        vm.imagens = [{'nome': '', 'url':''}];


        vm.addNewImage = function() {
            var newItemNo = vm.imagens.length+1;
            vm.imagens.push({'nome':'imagem'+newItemNo});
        };

        vm.removeImage = function() {
            var lastItem = vm.imagens.length-1;
            vm.imagens.splice(lastItem);
        };

        vm.salvar = function (anuncio) {
            anuncio.imagens = vm.imagens;
            vm.entry = new MainService(); //You can instantiate resource class
            vm.entry.data = anuncio;

            MainService.save(vm.entry, function(response) {
                toastr.info(response.result);
                if(response.result == "success"){

                    $rootScope.atualizarAnuncios();
                    vm.imagens = [{'nome': '', 'url':''}];
                    vm.anuncio = {};
                }else{
                    //do something
                }
            }); //saves an entry./

        };
        vm.close = function () {
            $uibModalInstance.close();
        };
    }

})();