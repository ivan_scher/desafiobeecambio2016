(function() {
  'use strict';

  angular
    .module('desafiobeecambio2016')
    .controller('MainController', MainController)
    .directive('fallbackSrc', function () {
      var fallbackSrc = {
        link: function postLink(scope, iElement, iAttrs) {
          iElement.bind('error', function() {
            angular.element(this).attr("src", iAttrs.fallbackSrc);
            angular.element(this).removeClass("normal");
            angular.element(this).addClass("error");
          });
        }
      }
      return fallbackSrc;
    });


  /** @ngInject */
  function MainController($rootScope, $uibModal, MainService ) {
    var vm = this;

    $rootScope.atualizarAnuncios = function(){
      MainService.query().$promise.then(function(data) {
        //$log.info(data);
        vm.anuncios = data;
      });  //query() returns all the entries
    }
    $rootScope.atualizarAnuncios();

    $rootScope.openModal = function(){
      $uibModal.open({
        animation: false,
        templateUrl: 'myModalContent.html',
        controller: 'ModalAddController',
        controllerAs: 'modal',
        size: 'lg'
      });
    }

  }

})();
